\section{Introduction}
GPU performance tuning is challenging because of the complexities in GPU
architecture and the interaction between threads in a massively parallel
execution environment (usually in the scale of thousands of threads). 

Previous GPU autotuning frameworks exploit different factors to improve performance. The PORPLE framework
\cite{Chen+:MICRO14} helps programmers determine which type of memory to use
with respect to data access patterns. Liu and colleagues \cite{Liu+:IPDPS09}
utilize input-sensitivity to help select the best program optimization
parameter for every input. Domain-specific knowledge is often used by compiler
designers to improve tuning efficiency. Anand and colleagues
\cite{VenKat+:PLDI15} explores program transformation parameters and
data representation for sparse matrix code optimization. The Halide
\cite{Ragan-Kelley+:PLDI13} framework tunes the locality and parallelism
parameters for image processing pipeline.

We introduce \emph{occupancy tuning}, a factor that is no less
important than any of the factors above for GPU program optimization.
\emph{Occupancy} \cite{CUDACguide} is defined as a measure of the number of threads \emph{active} at
one time on a GPU. A program running at two different
concurrency levels can have more than a 25-fold difference in running time, as shown in
Figure \ref{fig:occuimpact}. A program running at two occupancy levels with the
same performance can have as much as a 68.5\% difference in resource usage. 
Occupancy has a significant impact in both performance and resource usage efficiency. 



\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{figures/intro_occ_speed.pdf}
\caption{Runtime v.s. occupancy in imageDenoising on GTX680.}
\label{fig:occuimpact}
\end{figure}

The impact of occupancy is too important to neglect for GPU program tuning.  The
occupancy level can be tuned by controlling the
amount of on-chip memory assigned to every thread. The more resources assigned to
every thread, the lower occupancy is, vice versa.  Higher occupancy allows a large number of threads to run and
hide memory latency while at the same time increasing resource
contention and power consumption. Tuning occupancy is a challenging
problem, as the best occupancy level depends on multiple factors including program
characteristics, compile-time resource partitioning and dynamic program behavior. 

In this paper, we design and implement an autotuning framework that searches for
and determines the best occupancy level for any given program, such that the minimal amount of
occupancy and resources is used to achieve the best performance. Efficient occupancy
tuning requires two key components. First, it requires a model that
characterizes the relationship between a program's performance and
computation (and memory) concurrency of the active threads. Secondly, it requires an efficient memory
resource partition and allocation strategy to realize the desired occupancy. 

Previous work that 
addresses the GPU resource allocation problem does not systematically address the
occupancy tuning problem. The GPU performance model proposed by Hong and others \cite{Hong+:ISCA10} requires off-line
profiling and the results are based on an architecture simulator. It does not
proactively tune the occupancy or indicate how a desired occupancy level can be
obtained. Previous work \cite{Sampaio+:SBLP12} \cite{Hayes+:ICS14} alleviate GPU on-chip memory pressure. While alleviating
register pressure does indirectly increase occupancy, it is not necessarily true
that higher occupancy is always better than lower occupancy. Furthermore, both
works \cite{Sampaio+:SBLP12} \cite{Hayes+:ICS14} only utilize static compile-time
information and does not adapt to runtime program behavior.

We develop an {\bf o}ccupancy-o{\bf ri}ented tuning and {\bf on}-chip
resource management (\textsc{Orion}) framework. We are not aware of any prior
work that systematically explores the influence of occupancy
 on GPU program optimization and the relationship between occupancy tuning and
memory resource allocation. Our contributions are summarized as follows.
\begin{itemize}
\item \textsc{Orion} is the first fully automatic occupancy tuning framework
that balances single thread performance and concurrent performance. 
\item \textsc{Orion} combines static and dynamic occupancy tuning. We develop a
binary interface compiler that performs iterative compilation and delivers a
small set of binary versions corresponding to candidate occupancy levels, and a runtime tuner that chooses the best version to
run. 
\item The binary compiler generates \emph{optimal} memory resource allocation
for any given occupancy level using bi-partite graph model (Section
\ref{subsec:compiler}).
\item The runtime tuner adapts to dynamic program behavior and quickly selects
the best binary version to run, usually within 3 iterations (Section
\ref{subsec:runtime}).
\item Our \textsc{Orion} runs on real system and does not require off-line
profiling. In different benchmarks, it can achieve up to 1.57 times performance improvement, 
68.5\% memory resource saving, and 28.5\% power reduction with respect to the baseline of 
\emph{nvcc} code (Section
\ref{sec:eval}).   
\end{itemize} 

