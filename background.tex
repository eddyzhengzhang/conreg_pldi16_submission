\section{Background}

GPUs deliver high high performance via the use of massive multithreading through 
the single instruction multiple thread (SIMT) execution model. Every 
thread runs the same code on different input sets. GPU threads are organized into \emph{thread warps}.
A thread warp typically consists of 32 threads. A thread warp is the minimum
execution unit. Thread warps are further organized into
thread blocks. A GPU is composed of multiple SMs. One thread block runs on at most one
SM. The number of active threads on one SM is a multiple of thread block size. 

GPU on-chip memory includes registers and cache. {\em Shared memory} is the
software-managed cache in NVIDIA terminology. We use NVIDIA terminology
throughout the paper. Accesses to shared memory are explicitly managed
by software. Every active thread gets an even partition of register file and
shared memory. So that how much register and shared memory is used by every
thread determines how many active threads at one time (the occupancy).
The hardware-managed cache is L1/L2 cache. Unlike registers and shared memory,
hardware cache size does not impose a constraint on the occupancy.  

GPU \emph{occupancy} \cite{CUDACguide} is defined as the ratio between the
actual number of active thread warps
and the maximum number of thread warps the hardware can schedule. The occupancy
can be calculated using per-thread register usage, per-thread shared memory usage and
total register file and shared memory size. 

Assume in a program every thread uses $V_{reg}$ register space and $V_{smem}$ shared memory
space.  The total register file size is $N_{reg}$ and the total shared memory size
is $N_{smem}$. The maximum number of threads the hardware can schedule at one time is
$S_{max}$. The formula below gives the occupancy.

{
\footnotesize
%\scriptsize
\begin{equation}
\text{Occupancy} = {\text{Min}(\sfrac{N_{\text{reg}}}{V_{\text{reg}}},
\sfrac{N_{\text{smem}}}{V_{\text{smem}}})}/{S_{max}}.
\label{equ:mincon}
\end{equation}
}

Since the number of active threads need to be rounded up to a multiple of thread
block sizes, and the register partition need to be aligned according to register
bank size constraints, the occupancy may be smaller than above and need to be
further adjusted. We use the formula in NVIDIA occupancy calculator
\cite{CUDAOCCUCAL} to obtain the accurate occupancy.

A GPU program consists of both CPU code and GPU code. A function that actually
runs on the GPU is called a \emph{GPU kernel}. We perform occupancy tuning for GPU kernels
only.

For more recent GPU architectures, the size of shared memory and L1 cache can be dynamically configured in two or
three preset modes. In every mode, the total size of shared memory and L1 cache
remains the same. In Kepler architecture and above \cite{KeplerWhitePaper}, the L1 cache is only used for
thread-private local variables. For this type of architecture, we take L1 cache
into consideration in the \textsc{Orion} compiler by setting the cache bits in the binary instruction. 





