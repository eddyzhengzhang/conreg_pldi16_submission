
To realize a certain occupancy, we bound the number of registers and the
size of shared memory used per thread. We first represent a program in the
Static Single Assignment (SSA) form, in which every variable is defined once and
only once. Then we start assigning the SSA variables to register and shared
memory for a thread. Since every thread executes the same binary code,
allocating for one thread is equivalent to allocating for all the threads. 
We call the commensurate amount of space for a 4-byte
register in on-chip memory (including shared memory and cache) an 
\emph{on-chip memory slot}. An SSA variable can be placed into register,
shared memory, or L1 cache (via local memory). We describe the unified allocation algorithm for
on-chip memory, since these three types of on-chip memory are similar in that
there is negligible impact in performance.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{figures/singlealloc.pdf}
\caption{Single procedure multi-class allocation algorithm}
\label{fig:singleproc}
\end{figure}


\paragraph{Minimizing Space Requirement}
The on-chip memory space to store the SSA variables should be minimal since if
it does not fit into on-chip memory space, there will be spilling into off-chip DRAM
memory, which is significantly slower. Therefore the first thing we need to
optimize is the on-chip memory space needed for a set of SSA variables.  

Optimal register allocation for \emph{single procedure} has been studied extensively in
CPU literature, and the technique can be applied to on-chip
memory allocation.  We adopt the Chaitin-Briggs register allocator
\cite{Briggs+:TOPLAS94} and build a variant of it by taking into consideration the wide variables (64-bit, 96-bit and
128-bit) that need consecutive and aligned registers. We use a stack to track
the priority of variables to be allocated
(colored). Our single-procedure allocation algorithm is detailed in Figure \ref{fig:singleproc}.

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{figures/stacks.pdf}
\caption{Optimized v.s. unoptimized inter-procedure allocation. The benchmarks
are from Rodinia benchmark suite and CUDA SDK.}
\label{fig:interproc}
\end{figure}

However, there is limited inter-procedure allocation research on GPUs. While quite a few GPU
programs have no procedure calls, there are GPU programs that do have function
calls. Even if the source code lacks them, there might be
function calls in the binary. An example is the intrinsic division function,
which is implemented as a function call for GPU architecture. For instance, 
several programs in Rodinia \cite{Che+:IISWC09} benchmark suite call the
floating-point division function multiple times.

We propose an allocation algorithm for multi-procedure GPU kernel which is
optimal in terms of both space requirement and data movement requirement.

Aside from the occupancy tuning benefits, our algorithm is useful since it
shows that not all GPU sub-procedure calls need to be in-lined for better
performance \footnote{In GPU compilation, sub-procedure calls are often in-lined
for good performance} when on-chip memory can be used efficiently across
procedure calls. 

We describe our inter-procedure allocation algorithm as a {\em compressible
stack}. The idea of the {\em compressible stack} is simple. With \emph{compressible stack}, right before entering a sub-procedure, we
compress the stack by coalescing the used on-chip
slots such that the sub-procedure can use the maximum number of contiguous
on-chip memory slots. Right after the sub-procedure returns, we restore the
location of moved slots back to their original locations so that
the program can continue execution correctly.  

We show how the compressible stack works with an example in Figure \ref{fig:sl}.  In Figure
\ref{fig:sl} (a), we use the column
$S_i$ to represent an on-chip memory slot. After single procedure allocation, one or
more SSA variables are mapped to one on-chip memory slot. We show the live range
of every SSA variable using the vertical black bar in Figure \ref{fig:sl} (a). The
liveness information indicates the used/unused status of an on-chip memory slot $S_i$ at different call points in the
program. The left ``Program" column represents the code of the program including
the procedure calls. If no variable is mapped to an on-chip memory slot at an execution point, the slot is unused.
For instance, S3 is unused when \emph{foo2} is called in Figure
\ref{fig:sl} (a). 

In Figure \ref{fig:sl} (a), before calling \emph{foo1}, we move the slot
that contains variable ``var5" into the slot between the slots that contain variables ``var3"
and ``var1" so that we have larger contiguous free space for procedure foo1.
This is important since a sub-procedure uses contiguous stack space.
Right after the sub-procedure returns, we restore the location of variable
``var5" to resume execution. 

 We demonstrate that it is important to minimize space for
inter-procedure allocation by showing the performance difference between
space minimize version and the space unoptimized version  in Figure
\ref{fig:interproc}. The ``no-space minimization" bar corresponds to the
unoptimized version and the running time is normalized to the optimized one.

\paragraph{Minimizing Data Movement}
\label{subsect:minswap}
Above shows how to minimize on-chip memory space used across procedure calls.
It comes at the cost of increased data movements. Therefore, minimizing data movement is also important. 

In CPU single procedure allocation literature, there has been work that proposes to
trade-off data movement for space. The chordal graph coloring model \cite{Palsberg:CATS07} is a well
known model that minimizes register usage while increasing data movements for
removing  $phi-$functions. Later work by Hack and other further optimize data
movements caused by removal of $phi-$functions \cite{HackPLDI08}. However, there
is no such work in minimizing data movements for inter-procedure allocation. As
far as we know, our work is the first one that minimizes data movement for
inter-procedure on-chip memory allocation. 

\begin{figure}[t]
\centering
\includegraphics[width=0.5\textwidth]{figures/rs.pdf}
\caption{The impact of on-chip memory slot layout on the number of data
movements for compressible stack. Every column $S_i$ represents a slot. The
vertical blocks in the column represent the liveness
of the variable assigned to the slot. 
Bold arrows represent direction of data movement for entering or leaving a
sub-procedure. }
\label{fig:sl}
\end{figure}

We first show an example of how data movements can be reduced. Figure \ref{fig:sl} (b) is the same as Figure \ref{fig:sl}
(a) except that the addressing of the on-chip memory slots is different. For the
original layout in Figure \ref{fig:sl} (a) at the point call(foo1), the sub-procedure $foo1$ needs to
use three consecutive slots, thus var5 in S4 needs to be copied to
S2. In Figure \ref{fig:sl} (a), altogether three data movements are necessary
before entering three call points call(foo1), call(foo2) and call(foo3) as indicated by the 
three arc arrows for ``before call". However, if we place the variables of the
original $S2$ slot to the location of original $S4$,
$S3$ to $S2$, $S4$ to $S3$, as illustrated in Figure \ref{fig:sl} (b), the total
number of data movements is reduced to 1 at \emph{call(foo2)} point, while at
call(foo1) and call(foo2), all available on-chip memory slots are contiguous.

Our compressible stack optimizes the layout by changing the address of physical
on-chip memory slots. We provide a polynomial time algorithm that
finds the optimal address mapping. We achieve this by modeling the problem as a {\em maximum-weight
bipartite matching} problem \cite{intrograph00}.

\begin{table}
\scriptsize
\centering
\caption{Notations}
\begin{tabular}{|c|p{5.0cm}|} \hline
Notation & Description \\ \hline
$SS_i$ & set of variables mapped to the i-th stack slot \\
$SLOT_i$ & i-th slot from the bottom of the stack \\
$X_{ij}$ & mapping between $SS_i$ and $j$-th slot (0 or 1) \\
$L_{ik}$ & liveness of $SS_i$ at $k$-th sub-procedure call \\
$B_{k}$ & desired stack height at $k$-th sub-procedure call \\
$C_{ijk}$ & \# of swaps incurred by placing $SS_i$ at $j$-th slot for $k-$th sub-procedure\\
$W_{ij}$ & \# of swaps incurred by placing $SS_i$ at $j$-th slot \\
$N$ & \# of sub-procedure calls\\
$M$ & maximum \# of simultaneously live variables in this procedure\\
$P^{swap}_{k}$ & \# of swaps incurred because of $k$-th sub-procedure call\\ \hline
\end{tabular}
\label{tbl:not}
\end{table}

Let $N$ be the number of static sub-procedures calls in the procedure of interest. $M$ represents the
number of variable sets, $SS_i$, assigned during the single-procedure graph
coloring (allocation) process such that $i=0...M-1$ and each $SS_i$ is mapped to
one on-chip memory slot.

We denote the number of data movements incurred for entering/leaving the $k$-th procedure call as
$P^{mov}_{k}$. The objective is to find the physical on-chip memory slot each variable set $SS_i$ for $i=0...M-1$
is mapped to such that the total number of data movements is minimal.
 { $${\textbf{min}}~ T\_{mov} = \sum_{k=0}^{N-1}P^{mov}_{k}$$}

We model this problem as a {\em maximum-weight bipartite
matching} problem by making use of the following Theorem.

\begin{theorem}
With the notations defined in Table
\ref{tbl:not},
 in the minimal-mov-assignment (MMA) problem, the total number of data movements contributed by
placing an arbitrary variable set $SS_i$ at an arbitrary location $j-$th memory slot $SLOT_j$ across all $k$
immediate sub-procedures is a constant. We define this number as $W_{ij}$. Assume at the k-th
sub-procedure call we need to bound the
compressed caller stack to at most $B_k$ slots, we have:
{\footnotesize $$ W_{ij} = \sum_{k=0}^{N-1} C_{ijk}$$ }
while

 {\footnotesize~~~~~$C_{ijk} = 1$ if ($L_{ik}==1$ \&\& $j\geq B_k$), otherwise $C_{ijk} = 0$.}
\label{cor:weight}
\end{theorem}
\begin{proof}
Since a movement will be invoked if and only if there is an available stack slot
placed beyond the top of the after-compression stack, we can determine if a
placement of the $SS_i$ set will incur a movement by checking its location
(address) in the stack.

If and only if $SS_i$ is live during the lifetime of the $k$-th sub-procedure
(liveness indicated as $L_{ik}$ in Table \ref{tbl:not}), and placed at $j-$th slot ($j$ starts from
0) counting from the current procedure's stack bottom is greater than or equal to $B_k$, $j\geq
B_k$, the number of data movements invoked by placing variable set $SS_i$ at $SLOT_j$ for $k$-th sub-procedure
$C_{ijk}$ is 1; otherwise, $C_{ijk}$ is 0. Therefore, we get all the movement contributed by placing $SS_i$ 
into $SLOT_j$ by summing up $C_{ijk}$ for $k=0...N-1$. The theorem is thus proved.
\end{proof}

\begin{figure}[th]
\includegraphics[width=0.4\textwidth]{figures/bipartite.pdf}
\caption{Bipartite matching model for minimal-mov-assignment problem. $SS_i$ represents the i-th
set of variables that can be mapped to one on-chip memory slot, $SLOT_i$ represents the i-th
physical on-chip memory slot in the procedure of interest.}
\label{fig:bimatchexp}
\end{figure}

%(TODO, where is the table referenced?)

We then transform the problem into a bipartite matching problem. A bipartite graph is a graph whose nodes can be decomposed into two
disjoint sets such that no two nodes within the same set are adjacent as shown in Fig. \ref{fig:bimatchexp}. 
A perfect matching in a bipartite graph is a set of pairwise non-adjacent edges that covers every 
node in the graph.  A maximum weighted bipartite matching is one where 
the sum of the weights of the edges in the matching is maximal as shown in Fig.
\ref{fig:bimatchexp}.

We let one of the two disjoint
sets of nodes correspond to the sets of variables -- $SS_i$ for $i=0...M-1$. The
other set of nodes correspond to the memory slots $SLOT_0$...$SLOT_{M-1}$ from
the bottom of the stack. One edge connects between a variable set and a stack
location (address).

We set the weight of the edge between a set $SS_i$ and a $j$-th stack slot to 
negative ${W_{ij}}$ as defined in Theorem \ref{cor:weight}. This value is the
total number of movements
invoked by placing $SS_i$ at $SLOT_j$. Therefore, a maximum weighted matching will indicate a minimum
number of movements as indicated in Fig. \ref{fig:bimatchexp}.

We solve the maximum weighted bipartite matching problem using the modified Kuhn–Munkres algorithm
\cite{Munkres1957}, with O($M^3$) time complexity, with M being the total number of variable
sets. Once a matching is found, we can infer where every variable set can be placed.

Our model works for the case where we need not compress the stack to the minimal
size possible. For example, if the stack can be compressed to allow for four
free slots, but we only need three, then it is sufficient to compress the stack to
allow three slots.
Therefore, we avoid extra overhead from pointless stack compression movements. 
Let the parameter $B_k$ be the size the stack needs to be compressed to, such that 
$B_k$ is greater than the minimal possible compressed
stack size. Then the optimality and complexity results still hold.

We demonstrate the effectiveness of the data movement optimization Figure
\ref{fig:interproc}. The bars that correspond to the case of ``unoptimized data
movement minimization" are the case without data movement optimization. Note
that without data movement optimization, the performance might be even worse
than not doing stack compression again. Therefore, minimizing data movement is
extremely critical for minimal space optimization to work well (which is
critical for occupancy tuning).


