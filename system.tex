\section{\textsc{Orion} System Design}
The \textsc{Orion} system automatically tunes occupancy. It performs two level
occupancy selection -- one at compile-time and the other at runtime. The
\textsc{Orion} compiler identifies a set of candidate occupancy levels that
potentially contain the optimal occupancy levels, and generates
corresponding binary code versions. The compiler iteratively generates a set of
candidate code versions since the dynamic factors (such as inputs, control
divergence, irregular memory accesses) at runtime might affect the choice
of optimal occupancy level. The runtime adapts to complex program behavior and
selects the best code version in the candidate set with a performance feedback
loop. We show the
the cooperation of \textsc{Orion} compiler and runtime in Figure \ref{fig:orion}.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{figures/orionframework.pdf}
\caption{\textsc{Orion} compiler and runtime cooperation}
\label{fig:orion}
\end{figure}

\subsection{\textsc{Orion} Compiler}
\label{subsec:compiler}

The compiler identifies a set of candidate occupancy levels and
generate a multi-version binary program so that each GPU kernel corresponds to
multiple code versions. The compiler performs iterative occupancy selection and
compilation. The compiler selects and generates up to five different code
versions for one kernel function and for most cases $<$ three versions,
which helps the runtime quickly identify the best version to 
adopt for final execution. 

The compiler sets an initial occupancy level and determines the occupancy
tuning direction: increasing or decreasing based on the performance model in Section
\ref{subsubsec:testing}. The compiler then performs on-chip memory allocation for 
the kernel, assigning register and shared memory per-thread so that the desired
 occupancy is achieved.T he iterative compilation process stops when occupancy reaches 
its limit set by our performance model. During this
process, we save a set of generated kernel binary versions according to the performance
modeling metric in Section \ref{subsubsec:testing}. This iterative refinement process 
is shown in Figure \ref{fig:orion} (a).

There are two major stages in \textsc{Orion} compiler. 
\begin{itemize}
\item The \emph{realizing occupancy} stage (Section
\ref{subsubsec:realization}) ensures that the code generated to achieve a
certain occupancy level is efficient, for instance, avoiding excessive
spilling from on-chip memory (register and shared memory) to off-chip memory. 
\item The \emph{testing occupancy} stage (Section
\ref{subsubsec:testing}) ensures that we select a small set of kernel binaries with good occupancy levels and we rule out the binaries that
are unlikely to yield good computing efficiency.  
\end{itemize}

Both stages are important. The first stage is more challenging, since in the
second stage the total number of possible occupancy levels is usually less than 16 (the
maximum number of thread blocks a GPU SM can schedule). The search space is
small, and there is both static pruning and dynamic pruning. Our lightweight
performance model can typically prune the search space by at least half. However, how to
generate the most efficient code that corresponds to one occupancy level is more
challenging. Inefficient memory allocation can diminish the benefits of
concurrent execution and prevent us from really understanding the impact of
occupancy selection. Thus we focus on describing the memory allocation process in the
\emph{realizing occupancy} stage.  

\subsubsection{Realizing Occupancy}
\label{subsubsec:realization}

\input{occuAlloc}

\begin{figure}
\includegraphics[width=0.4\textwidth]{figures/staticselect.pdf}
\caption{Occupancy update algorithm}
\label{fig:occupdate}
\end{figure}

\subsubsection{Testing Occupancy}
\label{subsubsec:testing}
The \emph{occupancy testing} component checks the generated kernel binary and
determines if the occupancy needs to be further increased and decreased. If the
occupancy needs to be further updated, shown as the back loop in Figure
\ref{fig:orion}, the \emph{realizing occupancy} component will be invoked again
to generate a new kernel binary.  If the occupancy does not need to be further
updated, the previously checked
versions would be saved as the candidate set of kernel binaries for runtime
adaptation. Our compiler determines and generates a set of kernel
versions in which the optimal occupancy version is most likely to appear. 
We are able to narrow down the set of candidate kernel versions to
within five, and in most cases, less than three, making it easy for the
runtime component to choose the right kernel version.

We set the initial occupancy such that all SSA variables fit into registers or
the maximum number of registers per thread has been used. We call it the
\emph{original} version since this is the version for which we decide the tuning
direction (increasing or decreasing). The original
version is not necessarily the best version, but it is a safe version. It is
also the version that we can use to try multiple decreased occupancy levels without 
generating new code (since we can tune occupancy down by dynamically increasing shared memory
usage per thread). 
Once the initial occupancy is set, next we decide the direction of increasing or
decreasing occupancy in the iterative selection process. Once the direction is
determined, we keep increasing/decreasing the occupancy levels and testing the
generated code. We stop increasing/decreasing at a certain point if a
termination condition has been met according to our performance model below.  We show the detailed
occupancy \emph{testing and update} algorithm in Figure \ref{fig:occupdate}. In
Figure \ref{fig:occupdate}, we define another version called \emph{conservative version}
which is the version that all SSA variables fit into on-chip memory or maximum
on-chip memory usage per thread has been reached. 


We use two metrics to determine the direction of occupancy tuning at
compile-time. We also need to rule out the kernel versions that have made the memory or computation
component of GPU saturated. In another word, we determine the upper bound and
lower bound of occupancy tuning for trying at runtime. The analytical model
\cite{Hong+:ISCA09} uses off-line profiled information  including memory
throughput and dynamic instruction count to estimate the performance of a GPU
program. We utilize a lightweight model to determine how much
memory or computation parallelism has been reached. Our approach does not require
off-line profiling and is effective at finding the candidate set of occupancies.  

\paragraph{Metric 1: cycles per load} We develop a metric called \emph{cycles
per load} (CPL) to represent memory level parallelism. We calculate the interval
between the dispatching of two consecutive memory load instructions. We achieve
this by checking the control flow graph and find the immediate following load
instructions for every load. The interval is obtained by taking data dependence
into consideration. We average the interval in the unit of cycles and get CPL. 
For NVIDIA Kepler architecture and above, we calculate inter-load time distance 
by taking advantage of the dispatch intervals encoded in the 
binary code, which indicate how many cycles the current instruction
needs to wait after the last instruction has been issued.

The inverse of CPL, 1/CPL estimates how many memory load requests are sent in
one cycle on average. Assuming the memory controller can
handle at most three memory load requests in one cycle, the average CPL is $1/3$
cycle. If $CPL < 1/3$, in this case, the peak memory level parallelism is reached, so
increasing the number of active thread warps does not help performance and may
even worsen performance. In the \emph{testing and update} algorithm described in
Figure \ref{fig:occupdate}, we can stop increasing occupancy when $CPL_{thresh}$, 
the threshold for CPL has been hit, when condition on line 9 is false, or we
determine that the occupancy should be decreased from the original at the very beginning at line
3-4. 

\paragraph{Metric 2: max-live} We use a second metric called \emph{max-live}, which 
is equal to the number of registers necessary to hold all simultaneously live variables.
When this value is low, the amount of on-chip memory resource demand per-thread is also low, 
and thus a high occupancy is already reached (hitting the maximum number of
active threads that hardware can handle). For this type of applications, we
can start tuning only by decreasing the occupancy from the initial
\emph{original} occupancy. We set to \emph{max-live} to 32 (or 20) in our experiments,
which is the number registers needed to achieve the hardware maximum occupancy
level for Kepler (or Fermi) architecture. 

The max-live metric needs to be used in combination with the CPL metric. Even if a program 
has very high CPL, if the max-live is only 16 then increasing occupancy in the
search space is 
impossible. On the other hand, even if a program has extremely high max-live, a low CPL 
value indicates a heavy latency cost in addition to register spilling cost if we
want to increase occupancy. Only when both values 
are above their thresholds, then, is it worthwhile to start trying to increase occupancy.

\paragraph{Other metrics} We find that the previous two metrics are sufficient to determine 
initial direction of occupancy change. We use a third metric to set the
termination condition for occupancy updating. We call it 
\emph{consecutive dispatch interval} (CDI) and CDI represents computation level parallelism. The
dispatch interval is defined as the number of cycles between the dispatching of
two consecutive instructions. In the ideal case, at every cycle, one or two
instructions should be dispatched. However, it is not the case in practice. We
calculate the average dispatch interval based on instruction latency and data
dependence. Note that the dispatch interval does not take memory latency into
consideration, as the memory load latency is usually not fixed. CDI is useful
when we keep lowering the occupancy. It sets the lower bound of occupancy so
that enough active threads can keep the GPU computing processor pipeline. The
lower bound is set at line 17 of Figure \ref{fig:occupdate} based on instruction
issuing latency and the number of hardware schedulers.

\begin{figure}
\includegraphics[width=0.4\textwidth]{figures/dynamicselect.pdf}
\caption{Dynamic occupancy selection algorithm}
\label{fig:dynamicocc}
\end{figure}

Finally, we provide a fail-safe option in case the direction prediction at
compile-time is incorrect at runtime (although this has rarely happened in our
evaluation). We generate kernel codes in the increasing direction, and also determine the parameters for kernels 
in the decreasing direction. This way, in the rare event that our initial direction 
is wrong, we can try the other direction as a fail-safe. For programs where the kernel 
does not have enough iterations for dynamic tuning, we only compiler
conservative version.

\subsection{\textsc{Orion} Runtime}
\label{subsec:runtime}
Given the candidate set of kernel binaries generated by the compiler,
\textsc{ORION} runtime monitors kernel performance and dynamically selects the
best kernel versions. For a loop that calls the same kernel, in the first
iteration, we run the original kernel and in the second iteration we start
trying updated occupancy levels based on the predicted tuning
direction by \textsc{Orion} compiler. We also have a fail-safe version if the
compiler predicted tuning direction is wrong, though this fail-safe was not 
necessary in our experiments. The detailed algorithm is in Figure \ref{fig:dynamicocc}. 

In the dynamic occupancy tuning algorithm in Figure \ref{fig:dynamicocc}, in the
second iteration, if increasing occupancy is suggested, we increase the
occupancy by one level (adding one more thread block) from the conservative
version. If it runs
faster than the conservative one, then we keep increasing occupancy, one step
at a time. We stop increasing occupancy when we hit the upper bound set by our
compiler. If the increased occupancy kernel runs slower than the conservative
kernel, we revert back to the conservative kernel and stop adapting. In
most cases, we need less than three iterations to adapt to the right \emph{kernel} version.

For the case that decreased occupancy is suggested, as described in Figure
\ref{fig:dynamicocc}, we run the original kernel
in the first iteration and start decreasing occupancy level at the second iteration. While 
the decreased occupancy kernel runs less than 2\% slower than the original kernel,
we keep decreasing concurrency level until we hit the lower bound suggested by
the \textsc{Orion} compiler. 
 
Most GPU programs contain a loop around the GPU kernel of interest. If there is
no loop but there are enough threads for a kernel, we perform \emph{kernel
splitting}
\cite{Zhang+:ASPLOS11}. We split one kernel invocation into multiple invocations
such that, every  
invocation of the split kernel launches at maximum the number of active threads at
the current occupancy level so that the total threads across invocations is the same as the original kernel invocation. If there are less than 10 iterations even after kernel
splitting, we run the conservative kernel version.
