\section{Evaluation}
\label{sec:eval}
\textsc{Orion} compiler is a binary interface compiler. It takes a NVIDIA GPU
binary file as input, disassembles the binary, extracts high level program
information, constructs the intermediate representation (including SSA, call
graph and etc), transforms the IR, and outputs transformed binary that
contains multi-level occupancy kernel codes. \textsc{Orion} compiler is built with a
front end that parses NVIDIA GPU binary using the reverse-engineered
NVIDIA instruction set architecture (ISA) open-source project
\emph{asfermi}. We also extended \emph{asfermi} to support Kepler 
architecture. Instead of using PTX, we use hardware binary since, even
if PTX is close to binary, transformations performed at PTX level may not be
reflected at the binary level. 

While the \textsc{Orion} framework currently support NVIDIA Fermi and Kepler
architectures, it can also be extended to support other GPU architectures if
we add a new front-end and back-end to parse and rewrite the binary, since
the middle-end and the transformation algorithms remain the same.

\paragraph{Platform} We perform experiments on two different machine platforms. One is equipped with an NVIDIA GTX680 GPU.  It has CUDA
computing capability 3.0, 8 streaming multi-processors (SM) and 192 cores
on each SM and 1536 cores in total. Every streaming multi-processor
(SM) has 65536 registers. Each SM has 64KB of combined shared memory and L1 cache. The
maximal number of active thread warps on one SM is 64 and the maximal number of
active threads is 2048.

The second machine platform is configured with an NVIDIA Tesla C2075 GPU. It has 
14 streaming multi-processors (SM), 32 cores on each and 448 CUDA cores in total. It has CUDA 
computing capability 2.0. Each SM has 32768 registers and 64KB of combined shared memory
and cache. The maximal number of active thread warps on every SM is
48 and the maximal number of active threads is 1536. For both
platforms, each register is 4 bytes, with wide variables stored in aligned,
consecutive registers.  We refer to the first machine
configuration as {\em GTX680}, and the second one as {\em C2075}. 

\begin{table*}
\scriptsize
\centering
\caption{Detailed benchmark information.   
\emph{Reg\#} is the total number of registers needed to avoid spilling.  
\emph{Func\#} is the number of static function calls. 
\emph{UserSmem} indicates if there is user-allocated shared memory. 
\emph{OccDirect.} indicates occupancy tuning direction suggested by
\textsc{Orion} compiler. 
\emph{SelectAccu.} is the \textsc{Orion} selected occupancy divided by best
occupancy.
\emph{Reg.Save} shows percentage of the register file that is saved. 
}
\begin{tabular}{|c||c|c|c|c||c|c|c|c|} \hline 
Benchmark & Domain & Reg \# & Func \# & UserSmem & OccDirect. & SelectAccu. & Speedup &
Reg. Save \\
\hline \hline
cfd \cite{Che+:IISWC09} & Computational fluid dynam. & 63 & 36 & No & Inc & 1.00 & 1.10 & 0\% \\ \hline
dxtc \cite{CUDASDK} & Image processing & 49 & 11 & Yes & Inc & 0.86 & 1.15 & 0\% \\ \hline
FDTD3d \cite{CUDASDK} & Numerical analysis & 48 & 0 & Yes & Inc & 0.96 & 1.33 & 0\% \\ \hline
heartwall \cite{Che+:IISWC09} & Medical imaging & 34 & 10 & Yes & Inc & 1.00 & 1.01 & 0\% \\ \hline
hotspot \cite{Che+:IISWC09} & Temperature modeling & 37 & 6 & Yes & Inc & 0.98 & 1.06 & 0\% \\ \hline
imageDenoising \cite{CUDASDK} & Image processing   & 63 & 2 & Yes & Inc & 0.81 & 1.04 & 0\% \\ \hline
particles \cite{CUDASDK} & Simulation & 52 & 0 & No & Inc & 0.88 & 1.14 & 0\% \\ \hline
recursiveGaussian \cite{CUDASDK} & Numerical analysis & 42 & 21 & No & Inc & 0.86 & 1.03 & 0\% \\ \hline
backprop \cite{Che+:IISWC09} & Machine learning & 21 & 0 & No & Dec & 0.92 & 1.00 & 17.75\% \\ \hline
bfs \cite{Che+:IISWC09} & Graph traversal & 16 & 0 & No & Dec & 1.00 & 1.04 & 26.32\% \\ \hline
gaussian \cite{Che+:IISWC09} & Numerical analysis & 11 & 2 & No & Dec & 1.00 & 1.16 & 65.50\% \\ \hline
srad \cite{Che+:IISWC09} & Imaging application  2 & 20 & 7 & Yes & Dec & 1.00 & 1.00 & 10.08\% \\ \hline
streamcluster \cite{Che+:IISWC09} & Data mining & 18 & 0 & No & Dec & 1.00 & 1.03 & 18.33\% \\ \hline
\end{tabular}
\label{tbl:alldetails}
\end{table*}

\paragraph{Benchmarks} We evaluate the effectiveness of the \textsc{ORION} framework
on benchmarks shown in Table
\ref{tbl:alldetails}. These benchmarks are chosen to cover GPU programs from
various domains with different characteristics: high register pressure v.s. low register pressure, with function
calls v.s. without function calls, with user-defined shared memory v.s. without
user-defined shared memory. The benchmarks come from the Rodinia \cite{Che+:IISWC09}
benchmark suite and the CUDA Computing SDK \cite{CUDASDK}. For every benchmark,
we run with five common thread block sizes: 256, 384, 512, 768, and 1024 as possible.
For some benchmarks, the user-defined shared memory may prevent us from us using
all of these configurations, so we omit the ones that are not feasible.

\paragraph{Metrics} We evaluate the \textsc{ORION} occupancy tuning framework
using three different metrics. The first metric is on how good \textsc{Orion} is it at selecting
the right occupancy (compared with the best occupancy by exhaustive search). The second metric
is the performance impact of \textsc{Orion}: how it affects the performance
with respect to NVIDIA's compiler \emph{nvcc} (the most widely used GPU compiler
today). The third metric the resource efficiency of \emph{Orion}: whether it uses the minimal
amount of resources (registers) to achieve best performance. 
We discovered that there is also power/energy saving
when resources are saved, and so we also evaluate the energy savings after reduction of resource usage.

\begin{figure*}[htp]
\centering
\subfloat[Tesla C2075]{\includegraphics[width=0.4\textwidth]{figures/eval_optDiff_fermi.pdf}}
%\hfill %uncomment to leave space in-between
\subfloat[GTX680]{\includegraphics[width=0.4\textwidth]{figures/eval_optDiff_kepler.pdf}}
\caption{\textsc{Orion} selected kernel version and the best kernel version by
exhaustive search.}
\label{fig:optDiff}
\end{figure*}

\subsection{Occupancy Selection}
We first evaluate the occupancy selection accuracy of \textsc{Orion}. In Table
\ref{tbl:alldetails}, in the column of ``SelectAcc.", we show the ratio between the
 selected
occupancy by \textsc{Orion} and the best occupancy by exhaustive search. The results are
averaged over all thread block sizes. It can be seen from the table that most of
the time, \textsc{Orion} selects the right occupancy. When it does not select
the best one, it chooses an occupancy that is close to the best one. On average,
\textsc{ORION}'s selection is only 5.71\% away from the best concurrency. 

We show the performance and the resource efficiency for the \textsc{Orion} selected
occupancy in Figure \ref{fig:optDiff}. Here we normalize the register file usage
and performance over the that of the best version by exhaustive search and the
numbers are averaged across all thread block sizes. We define the 'best' version, determined 
via exhaustive search for comparison purpose, as the version with
best performance but the lowest occupancy level. The performance of the selected
occupancy is close that of the best one, on average only 3.11\% away from the
best running time. For \emph{cfd} on Tesla
C2075, although \textsc{Orion} selects the best occupancy, the performance is
slightly suboptimal. It is because the L1 cache setting we choose in
Orion's algorithm is always the largest L1 cache setting allowable at each occupancy level, 
while the exhaustive search includes all possible L1 cache configurations at every
occupancy level. In this case, the smaller cache configuration gives better performance. 
Detecting different cache configuration cases is possible, but could require trying up to three times as many 
versions of the kernel functions. Our current strategy works well for most applications, and so 
we will leave the search of the best cache configuration as a further optimization for our future work.  

In \textsc{Orion} dynamic selection, none of our benchmarks required more than
six iterations. The runtime adapts quickly to the best kernel version. On C2075 our benchmarks took 
2.96 iterations on average, and on GTX680 they took 2.69 iterations on average.

Most of our benchmarks has enough iterations for dynamic tuning, or else spawn enough threads that we 
can split the kernel into multiple iterations. The only exception to this is the particles benchmark. In addition to having only 1 iteration,
it spawns too few threads (32,768) for splitting the kernel to be worthwhile. As such, for this benchmark our 
framework always selects the conservative version of the kernel as defined in Section \ref{subsec:runtime}.

\begin{figure*}[htp]
\centering
\subfloat[Tesla C2075]{\includegraphics[width=0.4\textwidth]{figures/eval_incOcc_fermi.pdf}}
%\hfill %uncomment to leave space in-between
\subfloat[GTX680]{\includegraphics[width=0.4\textwidth]{figures/eval_incOcc_kepler.pdf}}
\caption{Normalized speedup of selected kernel compared to nvcc.}
\label{fig:incOcc}
\end{figure*}

\subsection{Performance Impact}
In the first metric, we use the best exhaustive searched occupancy as the
baseline. In the second metric, we use the code generated by \emph{nvcc} as
baseline. \emph{nvcc} is the CUDA compiler by NVIDIA, and does not perform
occupancy tuning. It always chooses the occupancy such that no register spilling
is needed or the maximum number of threads has been reached. While this
occupancy is the best case in a few scenarios, it is not necessarily always the
best case. We show that in most of the cases, either \textsc{Orion} selected
occupancy outperforms this naively selected occupancy or uses less resource but
yielding the same performance (within 2\%). We first show in Figure
\ref{fig:incOcc}, the performance of eight benchmarks compared with the original
binary compiled by {\em nvcc}. 
  
Among our benchmarks there are eight which can benefit from increased occupancy, identified 
in Table \ref{tbl:alldetails}. In Figure \ref{fig:incOcc}, the `max' bar is the
best speedup from all the block sizes that we tried, the `min' bar is the worst
speedup from all the block sizes that we tried, and the 'avg' value 
is the average speedup across all of the block sizes. Note that even in the worst
case, we do not perform worse than the original \emph{nvcc} generated code. For
those block sizes where \textsc{Orion} did not outperform nvcc, the reason is typically that
the thread block size is sufficiently large such that increasing occupancy level at 
all would add an excessive amount of spilling.
Overall, we achieve a maximum of 1.57 times speedup when compared with the nvcc
version, with an average speedup of 10.14\% on C2075 and 10.99\% on GTX680. The
average speedup for every benchmark is also shown in Table \ref{tbl:alldetails}.

The \emph{heartwall} benchmark is the only benchmark that shows little improvement. It is because of its large amount of 
pre-existing (user-defined) shared memory usage, which prevents \textsc{Orion} from increasing occupancy very far. 
Particles - the only benchmark in which we have insufficient iterations to perform dynamic tuning 
- also gets a good performance improvement with the conservative version
generated by \textsc{Orion} compiler. This demonstrates that significant speedup is 
achievable even with the static concurrency selection strategy. Moreover, the
\emph{particles} benchmark would likely be further improved if there were enough
loop iterations for dynamic tuning.

\begin{figure*}[htp]
\centering
\subfloat[Tesla C2075]{\includegraphics[width=0.4\textwidth]{figures/eval_lowOcc_fermi.pdf}}
%\hfill %uncomment to leave space in-between
\subfloat[GTX680]{\includegraphics[width=0.4\textwidth]{figures/eval_lowOcc_kepler.pdf}}
\caption{Minimal occupancy level for best performance.}
\label{fig:lowOcc}
\end{figure*}

\subsection{Resource Efficiency Impact}
In the third metric of resource efficiency, we still use \emph{nvcc} generated
code as baseline. We show the results of five benchmarks for which
\textsc{Orion} selected lower occupancy than \emph{nvcc} generated code's
occupancy while maintaining the same or better
performance as \emph{nvcc}'s code. For this set of benchmarks, we
can achieve the best performance with lower occupancy and thus non-trivial
register saving. The register saving, normalized runtime and normalized occupancy,
of these five benchmarks in Figure \ref{fig:lowOcc}. Everything is normalized to
the code compiled by {\em nvcc} and averaged across all the block sizes. Overall, we 
manage to decrease occupancy by 25.15\% on average, and register usage by 27.60\%.

It can be seen from Figure \ref{fig:lowOcc} that when lowering occupancy we
can still attain speedup on some benchmarks, potentially due to the decreased
resource contention that results from fewer active threads. We find that it occurs more noticeably on C2075, where the L1 cache is used for both global memory and local memory, than on GTX680 where the L1 cache is used exclusively for
thread-private local memory. Overall, we get an average speedup of 4.07\% for
these five benchmarks. We also list the register saving and speedup results in
Table \ref{tbl:alldetails}.

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{figures/eval_energy.pdf}
\caption{Energy usage of selected kernel.}
\label{fig:energy}
\end{figure}

Besides saving registers, lowering concurrency has the additional benefit of reducing power consumption, which we measured 
using NVIDIA's CUPTI API. GTX680 does not allow for power measurement in this 
manner, and so we show the energy savings for C2075, only, in Fig. \ref{fig:energy}. 
We are able to save energy in each of these benchmarks except backprop, where
the \textsc{Orion} selected version 
of the kernel on C2075 is at the original concurrency. Overall, we achieve an average of 13.15\% 
reduction in energy usage if we take program execution time into consideration.
The comparative energy saving in each benchmark corresponds, roughly, to how far we reduced the 
occupancy of each; this is a natural consequence of power usage decreasing as the number of 
active threads goes down.

We believe that with minor architectural changes, to turn off parts of the register file or some 
warp scheduling units, power could be further saved due to the decreased resource requirements 
of lower occupancy, but we tested our framework only on real hardware and therefore were not able 
to evaluate this. 


